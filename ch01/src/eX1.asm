; boot.asm
; OS Bootloader
; Author: Xsism
; Date:   2/8/03


[bits 16]
[org 0x7c00]

jmp boot                      ; jump over the data to our code

;-----------------------Data-----------------------;
;------------Variables---------------;
msg DB "BootSector Loaded",0  ; simple message
;----------End Variables-------------;

;------------Functions---------------;
print:
     lodsb                    ; laod ch from SI
     or al,al                 ; test if string is done
     jz end
     mov ah,0x0E              ; function to print
     mov bx,0x0007            ; white text
     mov cx,6                 ; str length
     int 0x10
     jmp print
end:
     ret
;----------End Functions-------------;

;---------------------End Data---------------------;
boot:
mov ax,cs                     ; setup segment and stack
mov ds,ax
mov ss,ax
mov sp,0x400                  ; 0x400 limit

mov si,msg                    ; print message     
call print
  
mov ax,0x0013 ;mode 13h
int 0x10

mov ah,0x0c
mov bh,0
mov al,2
mov cx,20
mov dx,20
int 0x10




; stop here
hlt



TIMES 510-($-$$) DB 0

SIGNATURE DW 0xAA55