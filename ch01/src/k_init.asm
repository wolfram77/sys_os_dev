[bits 32]        ; 32bit code here
[global start]   ; start is a global function
[extern _k_main] ; this is the kernel function
start:     

call _k_main     ; jump to k_main() in kernel.c

hlt              ; halt the cpu