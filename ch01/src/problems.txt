-----======Chapter Problems======-----
1: Create a simple bootsector that prints a message like, "Bootsector Loaded."

2: Create a bootsector using the code from problem 1 that enables the A20 gate,
   loads some data off the floppy disk and halts.

3: Create a bootsector that does all the above and loads the GDT, a Null IDT
   (optional), and enters Protected Mode and then halts.

-----======File List======-----
Note:all files in this list do the same as the file(s) above it

[following are in the src directory]
boot.asm- this is an example bootloader that loads the kernel from floppy to 0x100000 
eX1.asm - loads up and prints 'Bootsector Loaded'
eX2.asm - enables a20 and loads data off disk
eX3.asm - loads GDT and enters Pmode


boot.asm - this is the final bootloader that is used through-out the rest of this 
           tutorial.
k_init.asm - just the code that calls the kernel.